/*
// $Id: //open/util/resgen/src/org/eigenbase/xom/wrappers/LocationImpl.java#1 $
// Package org.eigenbase.xom is an XML Object Mapper.
// Copyright (C) 2008-2008 The Eigenbase Project
// Copyright (C) 2008-2008 Disruptive Tech
// Copyright (C) 2008-2008 LucidEra, Inc.
//
// This library is free software; you can redistribute it and/or modify it
// under the terms of the GNU Lesser General Public License as published by the
// Free Software Foundation; either version 2 of the License, or (at your
// option) any later version approved by The Eigenbase Project.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this library; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
package org.eigenbase.xom.wrappers;

import org.eigenbase.xom.Location;

/**
 * Location information.
 *
 * @author jhyde
 * @version $Id: //open/util/resgen/src/org/eigenbase/xom/wrappers/LocationImpl.java#1 $
 * @since Jun 17, 2008
 */

// End LocationImpl.java
