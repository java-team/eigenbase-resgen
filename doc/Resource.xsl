<?xml version='1.0'?>
<!--
// $Id: //open/util/resgen/doc/Resource.xsl#2 $
// package org.eigenbase.resgen is an i18n resource generator
// Copyright (C) 2005-2009 The Eigenbase Project
// Copyright (C) 2005-2009 SQLstream, Inc.
// Copyright (C) 2005-2009 LucidEra, Inc.
// Portions Copyright (C) 2000-2005 Kana Software, Inc. and others.
// All Rights Reserved.
//
// This library is free software; you can redistribute it and/or modify it
// under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation; either version 2.1 of the License, or
// (at your option) any later version.
//
// This library is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public
// License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this library; if not, write to the Free Software Foundation, Inc.,
// 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
  -->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/TR/WD-xsl">
<xsl:template match="/">

<TABLE BORDER="1">
<TR><TH>Static?</TH><TD><xsl:value-of select="resourceBundle/@static"/></TD></TR>
<TR><TH>Locale</TH><TD><xsl:value-of select="resourceBundle/locale"/></TD></TR>
<TR><TH>Exception class</TH><TD><xsl:value-of select="resourceBundle/@exceptionClassName"/></TD></TR>
</TABLE>

<P/>

<TABLE BORDER="1">
<TR>
<TH>Id</TH>
<TH ALIGN="LEFT">Name</TH>
<TH ALIGN="LEFT">Message text</TH>
</TR>
<xsl:for-each select="resourceBundle/exception">
<TR>
<TD><xsl:value-of select="@id"/></TD>
<TD><xsl:value-of select="@name"/></TD>
<TD><PRE><xsl:for-each select="text"><xsl:value-of select="text()"/></xsl:for-each></PRE></TD>
</TR>
</xsl:for-each>
</TABLE>
</xsl:template>
</xsl:stylesheet>
<!-- End Resource.xsl -->
